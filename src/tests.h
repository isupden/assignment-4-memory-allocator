#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H
void malloc_test();
void free_one_block_test();
void free_two_blocks_test();
void extend_old_region_test();
void not_extend_old_region_test();
#endif //MEMORY_ALLOCATOR_TESTS_H
