#include "tests.h"

int main() {
    malloc_test();
    free_one_block_test();
    free_two_blocks_test();
    extend_old_region_test();
    not_extend_old_region_test();
}
